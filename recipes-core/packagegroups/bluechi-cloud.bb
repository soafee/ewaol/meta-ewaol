SUMMARY = "Eclipse BlueChi Orchestration Integration"
DESCRIPTION = "Include BlueChi orchestration components in SOAFEE"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup

RDEPENDS:${PN} += "\ 
    ${VIRTUAL-RUNTIME_container_engine} \
    bluechi \
"
