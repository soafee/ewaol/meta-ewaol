# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Require inc file for Cloud DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES', 'ewaol-cloud', \
'conf/distro/include/ewaol-cloud.inc', '', d)}
