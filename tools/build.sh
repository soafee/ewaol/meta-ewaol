#!/bin/sh

TARGET="${1:-qemuarm64-secureboot}"
echo "Building EWAOL for ${TARGET}"

GIT=$(which git) || { echo "E: You must have git" && exit 1; }
KAS=$(which kas) || { echo "E: You must have kas" && exit 1; }

KAS_URL="${KAS_URL:-https://gitlab.com/Linaro/genesis/kas.git}"
KAS_BRANCH="${KAS_BRANCH:-scarthgap}"

set -ex

[ ! -d "kas" ] && ${GIT} clone --branch "${KAS_BRANCH}" --depth 1 "${KAS_URL}"

export DL_DIR="${DL_DIR:-${HOME}/yocto_cache/download}"
export SSTATE_DIR="${SSTATE_DIR:-${HOME}/yocto_cache/sstate}"
export KAS_BUILD_DIR="${KAS_BUILD_DIR:-${PWD}/build}"
mkdir -p "${KAS_BUILD_DIR}"

${KAS} build --provenance mode=max "kas/${TARGET}.yml"
