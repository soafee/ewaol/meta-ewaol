<!--
SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
affiliates <open-source-office@arm.com></text>

SPDX-License-Identifier: MIT
-->

## How to build

This example script builds an image using the EWAOL distribution with [kas][1].

```shell
curl -sSL https://gitlab.com/soafee/ewaol/meta-ewaol/-/raw/scarthgap/tools/build.sh | sh
```

Environment variables ``DL_DIR`` or ``SSTATE_DIR`` can be passed to override the
default values.

```shell
curl -sSL https://gitlab.com/soafee/ewaol/meta-ewaol/-/raw/scarthgap/tools/build.sh | SSTATE_DIR=/path/to/sstate-cache sh
```

## How to run

The example below shows how to run the default ``qemuarm64-secureboot`` machine
with [kas][1] and [QEMU][2].

```shell
kas shell kas/qemuarm64-secureboot.yml -c 'runqemu'
```

[1]: https://kas.readthedocs.io/en/latest/
[2]: https://docs.yoctoproject.org/5.0.2/dev-manual/qemu.html
