<!--
SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
affiliates <open-source-office@arm.com></text>

SPDX-License-Identifier: MIT
-->

# Edge Workload Abstraction and Orchestration Layer

The Edge Workload Abstraction and Orchestration Layer (EWAOL) project provides
users with a standards-based framework using containers for the deployment and
orchestration of applications on edge platforms. EWAOL is provided as the
meta-ewaol repository, which includes metadata for building EWAOL distribution
images via the Yocto Project.

It extends the Cassini distribution from [meta-cassini][1].

## How to build

This repository provides a configuration file to build an image using the EWAOL
distribution using [kas][2]. The distribution kas file must be paired with a BSP
layer for the ``MACHINE`` you want to target.

The example below shows a kas file to build EWAOL for ``qemuarm64-secureboot``
machine from [meta-arm][3] layer:

```yaml
header:
  version: 16
  includes:
    - repo: meta-ewaol
      file: kas/ewaol.yml

defaults:
  repos:
    branch: scarthgap

repos:
  meta-ewaol:
    url: https://gitlab.com/soafee/ewaol/meta-ewaol
    path: layers/meta-ewaol

  # The default configuration of Cassini requires meta-arm
  meta-arm:
    url: https://git.yoctoproject.org/meta-arm
    path: layers/meta-arm
    layers:
      meta-arm:
      meta-arm-toolchain:

# The machine is defined in meta-arm layer
machine: qemuarm64-secureboot
```

Such kas configuration files can be found under the [Genesis project][4].

[1]: https://cassini.readthedocs.io/en/latest/
[2]: https://kas.readthedocs.io/en/latest/
[3]: https://git.yoctoproject.org/meta-arm/tree/meta-arm/conf/machine/qemuarm64-secureboot.conf
[4]: https://gitlab.com/Linaro/genesis/kas/-/raw/scarthgap/qemuarm64-secureboot.yml
