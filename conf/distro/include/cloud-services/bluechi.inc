# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# set virtual runtime vars
VIRTUAL-RUNTIME_container_engine:${BASE_DISTRO} = "podman"

# add the necessary distro features 
DISTRO_FEATURES:append:${BASE_DISTRO} = " systemd overlayfs"

# required overlayfs config
OVERLAYFS_MOUNT_POINT[data] = "/var/lib/containers/storage/overlay"
OVERLAYFS_WRITABLE_PATHS[data] = "/var/lib/containers/storage/overlay" 
