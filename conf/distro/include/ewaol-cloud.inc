# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

VIRTUAL-RUNTIME_cloud_service ??= "bluechi-cloud"

include ${@bb.utils.contains(\
'VIRTUAL-RUNTIME_cloud_service','bluechi-cloud', \
'cloud-services/bluechi.inc', '', d)}
